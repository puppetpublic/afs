# Configuration and package installation for the AFS client and its
# supporting files and configuration.

# FIXME: Apparently Red Hat still uses Transarc paths?  This definitely needs
# to be fixed.  Work around it for right now.

# $use_dkms is a boolean.  If true, then the DKMS package will be used for
# OpenAFS, instead of the pre-built source package (which might not exist).
class afs (
  $use_dkms = false
) {
  $root = $::osfamily ? {
    'redhat' => '/usr/vice/etc',
    default  => '/etc/openafs'
  }

  # cacheinfo has to be provided elsewhere, either by the package
  # installation (ideal) or by the build, since it depends on whether
  # there's a chroot in use on the system and on what cache size one
  # wants.
  file {
    '/etc/filter-syslog/afs':
      source => 'puppet:///modules/afs/etc/filter-syslog/afs';
    "$root/ThisCell":
      source  => 'puppet:///modules/afs/etc/openafs/ThisCell',
      require => Package['openafs-client'];
    "$root/CellAlias":
      source  => 'puppet:///modules/afs/etc/openafs/CellAlias',
      require => Package['openafs-client'];
    "$root/CellServDB":
      source  => 'puppet:///modules/afs/etc/openafs/CellServDB',
      require => Package['openafs-client'];
  }

  case $::operatingsystem {
    /(?i:redhat|centos|OracleLinux)/: {
      file {
        '/etc/sysconfig/afs':    source => 'puppet:///modules/afs/etc/sysconfig/afs';
        # Not only does RHEL use transarc paths, it uses a dist file
        # to recreate cellservdb every time openafs-client init script
        # is run.
        "$root/CellServDB.dist": source => 'puppet:///modules/afs/etc/openafs/CellServDB';
      }

      package { 'openafs-client': ensure => present }
    }

    'debian': {
      if $::lsbdistcodename == 'wheezy' {
        # Use the backport version of OpenAFS
        file { '/etc/apt/preferences.d/openafs-client':
          content => template('afs/preferences.erb'),
          notify  => Exec['afs aptitude update'],
        }
        package { ['openafs-client', 'openafs-krb5']:
          ensure  => present,
          require =>
            [
              File['/etc/apt/preferences.d/openafs-client'],
              Exec['afs aptitude update'],
            ],
        }
        exec { 'afs aptitude update':
          command     => 'aptitude update',
          path        => '/bin:/usr/bin',
          refreshonly => true,
        }
      } else {
        package { ['openafs-client', 'openafs-krb5']: ensure  => present }
      }
    }

    'ubuntu': {
      # For Ubuntu releases where there's a backports version, use that
      if $::lsbdistcodename == 'xenial' {
        file { '/etc/apt/preferences.d/openafs-client':
          content => template('afs/preferences.erb'),
        }
        package { ['openafs-client', 'openafs-krb5', 'libpam-afs-session']:
          ensure  => present,
          require => File['/etc/apt/preferences.d/openafs-client'],
        }
        if ($use_dkms) {
          package { 'openafs-modules-dkms':
            ensure  => present,
            require => File['/etc/apt/preferences.d/openafs-client'],
          }
        } else {
          package { 'openafs-modules3':
            ensure  => present,
            require => File['/etc/apt/preferences.d/openafs-client'],
          }
        }
      }
      else {
        package { ['openafs-client', 'openafs-krb5', 'libpam-afs-session']: ensure => present }
        if ($use_dkms) {
          package { 'openafs-modules-dkms': ensure => present }
        } else {
          package { 'openafs-modules3': ensure => present }
        }
      }
    }
  }

  # AFS-related iptables rules.
  base::iptables::rule { 'afs':
    description => 'Allow AFS callbacks despite expired UDP sessions',
    source      => [ '171.64.7.0/24',
                     '171.64.17.0/24',
                     '171.67.16.0/23',
                     '171.67.20.0/24',
                     '171.67.22.0/24',
                     '171.67.24.0/23' ],
    protocol    => 'udp',
    port        => 7001,
  }

  # Increase the maximum number of root-created keyrings since they're
  # used for AFS PAGs.
  base::sysctl { 'kernel.keys.root_maxkeys': ensure => 5000 }
}

# Overriding class to allow AFS communications with off-campus servers.
class afs::global inherits afs {
  Base::Iptables::Rule['afs'] { source => '' }
}
