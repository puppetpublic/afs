# This is an override class that is used to create the AFS
# configuration for testcell.stanford.edu.

class afs::testcell inherits afs {
  File['/etc/openafs/CellAlias']{
    source => 'puppet:///modules/afs/CellAlias-testcell'
  }
  File['/etc/openafs/CellServDB']{
    source => 'puppet:///modules/afs/CellServDB-testcell'
  }
  File['/etc/openafs/ThisCell']{
    source => 'puppet:///modules/afs/ThisCell-testcell'
  }
}